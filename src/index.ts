import './styles/main.scss'
import ExampleService from './services/example.service'

const a = 4200;
const b = 'Bob';

const emp = new ExampleService(a, b);

// Create heading node
const heading = document.createElement('h1')
heading.textContent = `(${emp.num}) ${emp.name}`

// Append heading node to the DOM
const app = document.querySelector('#root')
app.append(heading)

// Create heading2 node
const heading2 = document.createElement('h2')
heading2.textContent = `${emp.name}'s number is ${emp.getNumber()}`
app.append(heading2)

