const paths = require('./paths')

const webpack = require('webpack')
const { merge } = require('webpack-merge')
const common = require('./webpack.common.js')

module.exports = merge(common, {
  // Set the mode to development or production
  mode: 'development',

  // Control how source maps are generated
  devtool: 'inline-source-map',

  // Spin up a server for quick development
  devServer: {
    historyApiFallback: true,
    contentBase: paths.build,
    open: true,
    compress: true,
    hot: true,
    port: 8081,
  },

  plugins: [
    // Only update what has changed on hot reload
    new webpack.HotModuleReplacementPlugin(),
  ],



  // Determine how modules within the project are treated
  module: {
    rules: [
      // Styles: Inject CSS into the head with source maps
      {
        test: /\.(scss|css)$/,
        use: [
          {
            loader: 'style-loader', // 4. Adds CSS to the DOM by injecting a `<style>` tag
          },
          {
            loader: 'css-loader', // 3. Interprets `@import` and `url()` like 
          },
          {
            loader: 'postcss-loader', // 2. Loader for webpack to process CSS with PostCSS
            options: {
              postcssOptions: {
                plugins: function () { // post css plugins, can be exported to postcss.config.js
                  return [
                    require('precss'),
                    require('autoprefixer')
                  ];
                }
              }
            }
          },
          {
            loader: 'sass-loader', // 1. Loads a SASS/SCSS file and compiles it to CSS
          },
        ],
      },
    ],
  },



})
