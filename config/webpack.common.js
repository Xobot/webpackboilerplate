const paths = require('./paths')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    entry: {
        vendor: paths.src + '/vendor.ts',
        main: paths.src + '/index.ts',
    },
    output: {
        path: paths.build,
        filename: '[name].bundle.js',
        publicPath: '/',
        assetModuleFilename: '[name].[hash][ext][query]'
    },

    // Customize the webpack build process
    plugins: [
        // Generates an HTML file from a template
        // Generates deprecation warning: https://github.com/jantimon/html-webpack-plugin/issues/1501
        new HtmlWebpackPlugin({
            title: 'Webpack Boilerplate',
            favicon: paths.src + '/images/favicon.png',
            template: paths.src + '/template.html',
            filename: 'index.html', // output file
        }),
    ],

    // Determine how modules within the project are treated
    module: {
        rules: [
            // Transpile to TypeScript
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: ['ts-loader']
            },
            {
                test: /\.html$/,
                use: ["html-loader"]
            },

            // https://webpack.js.org/blog/2020-10-10-webpack-5-release/#asset-modules
            // https://webpack.js.org/guides/asset-management/

            // Images: Copy image files to build folder
            { test: /\.(?:ico|gif|png|jpg|jpeg)$/i, type: 'asset/resource' },

            // Fonts and SVGs: Inline files
            { test: /\.(woff(2)?|eot|ttf|otf|svg|)$/, type: 'asset/inline' },
        ],
    },
    resolve: {
        extensions: ['.ts', '.js', '.tsx']
    },
}
